module gitlab.com/akishitara/k8scli

go 1.12

require (
	github.com/ghodss/yaml v1.0.0
	github.com/gin-gonic/gin v1.4.0
	k8s.io/api v0.0.0-20190620084959-7cf5895f2711
	k8s.io/apimachinery v0.0.0-20190612205821-1799e75a0719
	k8s.io/client-go v0.0.0-20190620085101-78d2af792bab
)
