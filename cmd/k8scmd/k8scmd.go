package main

import (
	"time"

	nexclient "gitlab.com/akishitara/k8scli/pkg/client"
)

func main() {
	//debugger.YamlPrint(nexclient.ActiveCronjobsAllGet())
	nexclient.Run()
	time.Sleep(3600 * time.Second)
}
